package com.missMirite.onlineShopProject.controller;

import com.missMirite.onlineShopProject.dto.BeautyProductsDTO;
import com.missMirite.onlineShopProject.service.impl.BeautyProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;

import static java.lang.String.format;

@RestController
@RequestMapping(value = "/beautyProducts")
public class BeautyProductController {

    private static final Logger LOGGER = Logger.getLogger(BeautyProductController.class.getName());
    @Autowired
    BeautyProductService beautyProductService;


    @GetMapping
    public List<BeautyProductsDTO> findAll() {
        return beautyProductService.findAll();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<BeautyProductsDTO> getById(@PathVariable("id") Integer id) {
        BeautyProductsDTO beautyProductsDTO = beautyProductService.getById(id);
        LOGGER.info(format("Found beauty product with id %s", id));
        return ResponseEntity.ok(beautyProductsDTO);
    }

    @GetMapping(value = "search/name/{name}")
    public ResponseEntity<List<BeautyProductsDTO>> getProductsByName(@PathVariable("name") String name) {
        List<BeautyProductsDTO> beautyProduct = beautyProductService.getProductsByName(name);
        LOGGER.info(format("Found beauty product with name %s", name));
        return ResponseEntity.ok(beautyProduct);
    }

    @GetMapping(value = "search/manufacturer name/{name}")
    public ResponseEntity<List<BeautyProductsDTO>> getProductsByManufacturerName(@PathVariable("name") String name) {
        List<BeautyProductsDTO> beautyProduct = beautyProductService.getProductsByManufacturerName(name);
        LOGGER.info(format("Found beauty product with manufacturer %s", name));
        return ResponseEntity.ok(beautyProduct);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BeautyProductsDTO> create(@RequestBody BeautyProductsDTO beautyProduct) {
        LOGGER.info(format("Trying to create beauty product with name '%s' and manufacturer '%s'",
                beautyProduct.getProductName(), beautyProduct.getManufacturerName()));
        BeautyProductsDTO createdBeautyProduct = beautyProductService.create(beautyProduct);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/save")
                .buildAndExpand(createdBeautyProduct.getId())
                .toUri();
        LOGGER.info(format("Beauty product with id %s is created", createdBeautyProduct.getId()));
        return ResponseEntity.created(uri).body(createdBeautyProduct);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BeautyProductsDTO> update(@PathVariable("id") Integer id,
                                                    @RequestBody BeautyProductsDTO beautyProductsDTO) {
        BeautyProductsDTO createdBeautyProduct = beautyProductService.update(id, beautyProductsDTO);
        LOGGER.info(format("Beauty product with id %s is updated", createdBeautyProduct.getId()));
        return ResponseEntity.ok(createdBeautyProduct);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Integer id) {
        try {
            beautyProductService.delete(id);
            LOGGER.info(format("Beauty product with id %s is deleted", id));
            return ResponseEntity.ok(format("Beauty product with id %s was successfully deleted", id));
        } catch (EmptyResultDataAccessException e) {
            LOGGER.severe(format("Cant delete because there is no beauty product with id %s", id));
            return ResponseEntity.notFound().build();
        }
    }

}


