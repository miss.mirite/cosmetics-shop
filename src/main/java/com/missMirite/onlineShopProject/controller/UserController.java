package com.missMirite.onlineShopProject.controller;

import com.missMirite.onlineShopProject.dto.UserDTO;
import com.missMirite.onlineShopProject.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

import static java.lang.String.format;

@RestController
@RequestMapping(value = "/users")
public class UserController {
    private static final Logger LOGGER = Logger.getLogger(UserController.class.getName());

    @Autowired
    UserService userService;

    @GetMapping
    public List<UserDTO> findAll() {
        return userService.findAll();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDTO> getById(@PathVariable("id") Integer id) {
        UserDTO userDTO = userService.getById(id);
        LOGGER.info(format("Found user with id %s", id));
        return ResponseEntity.ok(userDTO);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> update(@PathVariable("id") Integer id,
                                          @RequestBody UserDTO userDTO) {
        UserDTO createdUser = userService.update(id, userDTO);
        LOGGER.info(format("User with id %s is updated", createdUser.getId()));
        return ResponseEntity.ok(createdUser);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Integer id) {
        try {
            userService.delete(id);
            LOGGER.info(format("User with id %s is deleted", id));
            return ResponseEntity.ok(format("User with id %s was successfully deleted", id));
        } catch (EmptyResultDataAccessException e) {
            LOGGER.severe(format("Cant delete because there is no user with id %s", id));
            return ResponseEntity.notFound().build();
        }
    }

}
