package com.missMirite.onlineShopProject.controller;

import com.missMirite.onlineShopProject.dto.OrdersDTO;
import com.missMirite.onlineShopProject.model.ShoppingCart;
import com.missMirite.onlineShopProject.service.impl.OrderService;
import com.missMirite.onlineShopProject.service.impl.ShoppingCartService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.logging.Logger;

import static java.lang.String.format;

@RestController
@RequestMapping(value = "/orders")
public class OrderController {

    private static final Logger LOGGER = Logger.getLogger(OrderController.class.getName());

    @Autowired
    OrderService orderService;

    @Autowired
    ShoppingCartService shoppingCartService;

    @PostMapping(value = "/save")
    public ResponseEntity<OrdersDTO> createNewOrder(HttpSession session) {
        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute("cart");
        OrdersDTO createdOrder = orderService.create(shoppingCart);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/save")
                .buildAndExpand(createdOrder.getId())
                .toUri();
        LOGGER.info(format("Order with id %s is created", createdOrder.getId()));
        shoppingCartService.deleteCart(session);
        return ResponseEntity.created(uri).body(createdOrder);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<OrdersDTO> getById(@PathVariable("id") Integer id) {
        OrdersDTO ordersDTO = orderService.getById(id);
        LOGGER.info(format("Found order with id %s", id));
        return ResponseEntity.ok(ordersDTO);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Integer id) {
        try {
            orderService.delete(id);
            LOGGER.info(format("Order with id %s is deleted", id));
            return ResponseEntity.ok(format("Order with id %s was successfully deleted", id));
        } catch (EmptyResultDataAccessException e) {
            LOGGER.severe(format("Cant delete because there is no order with id %s", id));
            return ResponseEntity.notFound().build();
        }
    }
}
