package com.missMirite.onlineShopProject.controller;

import com.missMirite.onlineShopProject.dto.BeautyProductsDTO;
import com.missMirite.onlineShopProject.model.ShoppingCart;
import com.missMirite.onlineShopProject.service.interfaces.IShoppingCartService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

import static java.lang.String.format;

@RestController
@RequestMapping(value = "/shoppingCart")
public class ShoppingCartController {

    private static final Logger LOGGER = Logger.getLogger(ShoppingCartController.class.getName());
    private static final String LOGLINE = "Product with id %s was added to the shopping cart";

    @Autowired
    IShoppingCartService shoppingCartService;

    @PostMapping("/addProduct")
    public ResponseEntity<String> addToCart(HttpSession session, @RequestBody Integer productId) {
        ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
        if (cart == null) {
            cart = new ShoppingCart();
            session.setAttribute("cart", cart);
        }
        shoppingCartService.addItemToCart(cart, productId);
        LOGGER.info(format(LOGLINE, productId));
        return ResponseEntity.ok(format(LOGLINE, productId));
    }

    @DeleteMapping("/deleteProduct")
    public void removeFromCart(HttpSession session, @RequestBody BeautyProductsDTO product) {
        ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
        if (cart == null) {
            cart = new ShoppingCart();
            session.setAttribute("cart", cart);
        }
        cart.addItem(product);
    }

    @GetMapping("/getShoppingCart")
    public ResponseEntity<ShoppingCart> getShoppigCart(HttpSession session) {
        ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
        if (cart == null) {
            cart = new ShoppingCart();
            session.setAttribute("cart", cart);
        }
        return ResponseEntity.ok(cart);
    }
}
