package com.missMirite.onlineShopProject.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import static org.springframework.beans.BeanUtils.copyProperties;

public class DataPopulator {

    private DataPopulator() {
    }

    public static <T> T populateNew(final Object source, Class<T> returnType) {
        T target;
        try {
            target = returnType.getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(source, target);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return target;
    }
}
