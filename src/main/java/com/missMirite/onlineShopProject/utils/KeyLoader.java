package com.missMirite.onlineShopProject.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static java.lang.String.format;


@Component
public class KeyLoader {

    @Value("${spring.security.private-key}")
    private String privateKey;

    @Value("${spring.security.public-key}")
    private String publicKey;

    public PrivateKey loadPrivateKey() {
        try {
            return loadPemRsaPrivateKey(privateKey);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public PublicKey loadPublicKey() {
        try {
            return loadPemRsaPublicKey(publicKey);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * This method loads a file from the classpath and returns it as a String.
     *
     * @param fileName
     * @return
     * @throws IOException
     */
    private String readFile(final String fileName) throws IOException {
        final File file = new File(getClass().getClassLoader().getResource(fileName).getFile());

        return new String(Files.readAllBytes(file.toPath()));
    }

    /**
     * This methos load the RSA private key from a PKCS#8 PEM file.
     *
     * @param pemFilename
     * @return
     * @throws Exception
     */
    private PrivateKey loadPemRsaPrivateKey(String pemFilename) throws Exception {
        String pemString = readFile(pemFilename);
        String privateKeyPEM = prepareKeyToEncode(pemString, "PRIVATE");
        byte[] encoded = Base64.getDecoder().decode(privateKeyPEM.trim());
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        return keyFactory.generatePrivate(keySpec);
    }

    /**
     * This methos load the RSA public key from a PKCS#8 PEM file.
     *
     * @param pemFilename
     * @return
     * @throws Exception
     */
    private PublicKey loadPemRsaPublicKey(String pemFilename) throws Exception {
        String pemString = readFile(pemFilename);
        String privateKeyPEM = prepareKeyToEncode(pemString, "PUBLIC");
        byte[] encoded = Base64.getDecoder().decode(privateKeyPEM.trim());
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
        return keyFactory.generatePublic(keySpec);
    }

    private String prepareKeyToEncode(String key, String keyType) {
        return key.replace(format("-----BEGIN %s KEY-----", keyType), "")
                .replace(format("-----END %s KEY-----", keyType), "")
                .replaceAll("\\s", "");
    }
}
