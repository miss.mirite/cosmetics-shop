package com.missMirite.onlineShopProject.repository;

import com.missMirite.onlineShopProject.model.BeautyProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BeautyProductsRepository extends JpaRepository <BeautyProduct, Integer>{


}
