package com.missMirite.onlineShopProject.repository;

import com.missMirite.onlineShopProject.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}
