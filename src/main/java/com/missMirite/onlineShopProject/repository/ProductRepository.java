package com.missMirite.onlineShopProject.repository;

import com.missMirite.onlineShopProject.model.BeautyProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<BeautyProduct, Integer> {

    @Query(value = "SELECT * FROM onlineShop WHERE product_name =?", nativeQuery = true)
    List<BeautyProduct> getBeautyProductByName(String name);

    @Query(value = "SELECT * FROM onlineShop WHERE manufacturer_name =?", nativeQuery = true)
    List<BeautyProduct> getBeautyProductByManufacturer(String manufacturer);
}
