package com.missMirite.onlineShopProject.repository;

import com.missMirite.onlineShopProject.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserRepository extends JpaRepository<User, Integer> {
    default UserDetails findUserByEmail(String email) {
        return findAll()
                .stream()
                .filter(u -> u.getUsername().equals(email)).findFirst().orElseThrow(
                        () -> new UsernameNotFoundException("No user with this username!")
                );
    }
}
