package com.missMirite.onlineShopProject.service.impl;

import com.missMirite.onlineShopProject.dto.OrdersDTO;
import com.missMirite.onlineShopProject.model.BeautyProduct;
import com.missMirite.onlineShopProject.model.Order;
import com.missMirite.onlineShopProject.model.ShoppingCart;
import com.missMirite.onlineShopProject.repository.OrderRepository;
import com.missMirite.onlineShopProject.service.interfaces.IOrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;

import static com.missMirite.onlineShopProject.utils.DataPopulator.populateNew;

@Service
public class OrderService implements IOrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ShoppingCartService shoppingCartService;
    LocalDateTime dateTime = LocalDateTime.now();

    @Override
    public OrdersDTO getById(Integer id) {
        Order order = orderRepository.findById(id).get();
        OrdersDTO orderDTO = new OrdersDTO();
        BeanUtils.copyProperties(order, orderDTO);
        return orderDTO;
    }

    @Override
    public OrdersDTO create(ShoppingCart shoppingCart) {
        Order entity = new Order();
        List<BeautyProduct> productEntities = shoppingCart.getCartItems().stream()
                .map(dto -> populateNew(dto, BeautyProduct.class))
                .toList();
        entity.setOrderItems(productEntities);
        entity.setOrderDate(dateTime);
        Order entity1 = orderRepository.saveAndFlush(entity);
        OrdersDTO dto = new OrdersDTO();
        BeanUtils.copyProperties(entity1, dto);
        return dto;
    }

    @Override
    public OrdersDTO update(Integer id, OrdersDTO order) {
        Order orderFromDB = orderRepository
                .findById(id).orElseThrow(() -> new NoSuchElementException("No order with id " + id));
        BeanUtils.copyProperties(orderFromDB, order);
        return order;
    }

    @Override
    public void delete(Integer id) {
        orderRepository.deleteById(id);
    }
}
