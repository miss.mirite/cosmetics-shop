package com.missMirite.onlineShopProject.service.impl;

import com.missMirite.onlineShopProject.dto.BeautyProductsDTO;
import com.missMirite.onlineShopProject.model.ShoppingCart;
import com.missMirite.onlineShopProject.repository.BeautyProductsRepository;
import com.missMirite.onlineShopProject.model.BeautyProduct;
import com.missMirite.onlineShopProject.model.User;
import com.missMirite.onlineShopProject.service.interfaces.IShoppingCartService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

import static com.missMirite.onlineShopProject.utils.DataPopulator.populateNew;

@Service
public class ShoppingCartService implements IShoppingCartService {

    @Autowired
    BeautyProductsRepository beautyProductsRepository;


    @Override
    public ShoppingCart getShoppingCartByUser(User user) {
        return null;
    }


    @Override
    public void addItemToCart(ShoppingCart shoppingCart, Integer id) {
        BeautyProduct beautyProduct = beautyProductsRepository.findById(id).get();
        shoppingCart.addItem(populateNew(beautyProduct, BeautyProductsDTO.class));
    }

    @Override
    public void removeItemFromCart(ShoppingCart shoppingCart, BeautyProduct product) {

    }

    @Override
    public void deleteCart(HttpSession session) {
        session.removeAttribute("cart");
    }

    @Override
    public BigDecimal calculateTotalPrice(ShoppingCart shoppingCart) {
        return null;
    }

    @Override
    public List<BeautyProduct> getCartItems(ShoppingCart shoppingCart) {
        return null;
    }
}
