package com.missMirite.onlineShopProject.service.impl;

import com.missMirite.onlineShopProject.dto.UserDTO;
import com.missMirite.onlineShopProject.model.User;
import com.missMirite.onlineShopProject.repository.UserRepository;
import com.missMirite.onlineShopProject.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.springframework.beans.BeanUtils.copyProperties;

@Service
public class UserService implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public List<UserDTO> findAll() {
        List<User> users = userRepository.findAll();
        List<UserDTO> userDTOS = new ArrayList<>();
        for (User user : users) {
            UserDTO dto = new UserDTO();
            copyProperties(user, dto);
            userDTOS.add(dto);
        }
        return userDTOS;
    }

    @Override
    public UserDTO getById(Integer id) {
        User user = userRepository.findById(id).get();
        UserDTO userDTO = new UserDTO();
        copyProperties(user, userDTO);
        return userDTO;
    }

    @Override
    public UserDTO create(UserDTO userDTO) {
        User entity = new User();
        copyProperties(userDTO, entity);
        User entity1 = userRepository.saveAndFlush(entity);
        UserDTO dto = new UserDTO();
        copyProperties(entity1, dto);
        return dto;
    }

    @Override
    public UserDTO update(Integer id, UserDTO userDTO) {
        User userFromDB = userRepository
                .findById(id).orElseThrow(() -> new NoSuchElementException("No user with id " + id));
        copyProperties(userFromDB, userDTO);
        return userDTO;
    }

    @Override
    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

}
