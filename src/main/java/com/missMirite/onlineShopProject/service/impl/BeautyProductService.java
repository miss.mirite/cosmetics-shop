package com.missMirite.onlineShopProject.service.impl;

import com.missMirite.onlineShopProject.dto.BeautyProductsDTO;
import com.missMirite.onlineShopProject.model.BeautyProduct;
import com.missMirite.onlineShopProject.repository.ProductRepository;
import com.missMirite.onlineShopProject.service.interfaces.IBeautyProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.missMirite.onlineShopProject.utils.DataPopulator.populateNew;
import static org.springframework.beans.BeanUtils.copyProperties;


@Service
public class BeautyProductService implements IBeautyProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<BeautyProductsDTO> findAll() {
        List<BeautyProduct> products = productRepository.findAll();
        List<BeautyProductsDTO> productDTOs = new ArrayList<>();
        for (BeautyProduct product : products) {
            BeautyProductsDTO dto = new BeautyProductsDTO();
            copyProperties(product, dto);
            productDTOs.add(dto);
        }
        return productDTOs;
    }

    @Override
    public BeautyProductsDTO getById(Integer id) {
        BeautyProduct product = productRepository.findById(id).get();
        BeautyProductsDTO productDTO = new BeautyProductsDTO();
        copyProperties(product, productDTO);
        return productDTO;
    }

    @Override
    public BeautyProductsDTO create(BeautyProductsDTO beautyProduct) {
        BeautyProduct entity = new BeautyProduct();
        copyProperties(beautyProduct, entity);
        BeautyProduct entity1 = productRepository.saveAndFlush(entity);
        BeautyProductsDTO dto = new BeautyProductsDTO();
        copyProperties(entity1, dto);
        return dto;
    }

    @Override
    public BeautyProductsDTO update(Integer id, BeautyProductsDTO beautyProduct) {
        BeautyProduct productFromDB = productRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("No product with id " + id));
        copyProperties(beautyProduct, productFromDB);
        return populateNew(productRepository.saveAndFlush(productFromDB), BeautyProductsDTO.class);
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);
    }

    @Override
    public List<BeautyProductsDTO> getProductsByName(String name) {
        List<BeautyProduct> products = productRepository.getBeautyProductByName(name);
        List<BeautyProductsDTO> productDTOs = new ArrayList<>();
        for (BeautyProduct product : products) {
            BeautyProductsDTO dto = new BeautyProductsDTO();
            copyProperties(product, dto);
            productDTOs.add(dto);
        }
        return productDTOs;
    }

    @Override
    public List<BeautyProductsDTO> getProductsByManufacturerName(String name) {
        List<BeautyProduct> products = productRepository.getBeautyProductByManufacturer(name);
        List<BeautyProductsDTO> productDTOs = new ArrayList<>();
        for (BeautyProduct product : products) {
            BeautyProductsDTO dto = new BeautyProductsDTO();
            copyProperties(product, dto);
            productDTOs.add(dto);
        }
        return productDTOs;
    }
}
