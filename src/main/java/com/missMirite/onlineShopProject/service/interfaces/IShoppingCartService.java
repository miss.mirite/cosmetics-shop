package com.missMirite.onlineShopProject.service.interfaces;

import com.missMirite.onlineShopProject.model.BeautyProduct;
import com.missMirite.onlineShopProject.model.ShoppingCart;
import com.missMirite.onlineShopProject.model.User;
import jakarta.servlet.http.HttpSession;

import java.math.BigDecimal;
import java.util.List;

public interface IShoppingCartService {
//    ShoppingCart create (User user);

    ShoppingCart getShoppingCartByUser(User user);

    void addItemToCart(ShoppingCart shoppingCart, Integer productId);

    void removeItemFromCart(ShoppingCart shoppingCart, BeautyProduct product);

    void deleteCart(HttpSession session);

    BigDecimal calculateTotalPrice(ShoppingCart shoppingCart);

    List<BeautyProduct> getCartItems(ShoppingCart shoppingCart);

}
