package com.missMirite.onlineShopProject.service.interfaces;

import com.missMirite.onlineShopProject.dto.OrdersDTO;
import com.missMirite.onlineShopProject.model.ShoppingCart;

public interface IOrderService {

//    List<OrderProduct> findAll();

    OrdersDTO getById(Integer id);

    OrdersDTO create(ShoppingCart shoppingCart);

    OrdersDTO update(Integer id, OrdersDTO order);

    void delete(Integer id);
}
