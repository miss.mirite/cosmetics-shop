package com.missMirite.onlineShopProject.service.interfaces;

import com.missMirite.onlineShopProject.dto.UserDTO;

import java.util.List;

public interface IUserService {
    List<UserDTO> findAll();

    UserDTO getById(Integer id);

    UserDTO create(UserDTO userDTO);

    UserDTO update(Integer id, UserDTO userDTO);

    void delete(Integer id);

}
