package com.missMirite.onlineShopProject.service.interfaces;

import com.missMirite.onlineShopProject.dto.BeautyProductsDTO;
import com.missMirite.onlineShopProject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface IBeautyProductService {

    List<BeautyProductsDTO> findAll();

    BeautyProductsDTO getById(Integer id);

    BeautyProductsDTO create(BeautyProductsDTO beautyProduct);

    BeautyProductsDTO update(Integer id, BeautyProductsDTO beautyProduct);

    void delete(Integer id);

    List<BeautyProductsDTO> getProductsByName(String name);

    List<BeautyProductsDTO> getProductsByManufacturerName(String name);
}
