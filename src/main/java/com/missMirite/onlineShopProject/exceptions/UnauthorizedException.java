package com.missMirite.onlineShopProject.exceptions;

public class UnauthorizedException extends RuntimeException{

    public UnauthorizedException() {
        super();
    }

    public UnauthorizedException(String s) {
        super(s);
    }
}
