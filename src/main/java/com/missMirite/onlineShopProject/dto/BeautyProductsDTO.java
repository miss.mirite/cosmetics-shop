package com.missMirite.onlineShopProject.dto;

import java.math.BigDecimal;
import java.util.Objects;

public class BeautyProductsDTO {
    private Integer id;

    private String productName;

    private String manufacturerName;

    private String productType;

    private BigDecimal price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "BeautyProductsDTO{" +
                "productName='" + productName + '\'' +
                ", manufacturerName='" + manufacturerName + '\'' +
                ", productType='" + productType + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BeautyProductsDTO bookDTO = (BeautyProductsDTO) o;
        return productName.equals(bookDTO.productName) && manufacturerName.equals(bookDTO.manufacturerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productName, manufacturerName);
    }
}
