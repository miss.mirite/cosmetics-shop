package com.missMirite.onlineShopProject.dto;

import com.missMirite.onlineShopProject.model.BeautyProduct;
import com.missMirite.onlineShopProject.model.OrderStatus;
import com.missMirite.onlineShopProject.model.User;

import java.time.LocalDateTime;
import java.util.List;

public class OrdersDTO {
    private Integer id;

    private User user;

    private OrderStatus status;

    private LocalDateTime orderDate;

    private List<BeautyProduct> orderItems;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public List<BeautyProduct> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<BeautyProduct> orderItems) {
        this.orderItems = orderItems;
    }

}
