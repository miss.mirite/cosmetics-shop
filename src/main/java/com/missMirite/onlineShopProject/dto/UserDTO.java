package com.missMirite.onlineShopProject.dto;

import lombok.Data;

import java.util.Objects;

@Data
public class UserDTO {

    private Integer id;

    private String firstName;

    private String lastName;

    private String email;

    private Boolean enabled;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return email.equals(userDTO.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}
