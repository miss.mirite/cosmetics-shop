package com.missMirite.onlineShopProject.auth;

import com.missMirite.onlineShopProject.config.JwtService;
import com.missMirite.onlineShopProject.dto.UserDTO;
import com.missMirite.onlineShopProject.exceptions.UserAlreadyExistsException;
import com.missMirite.onlineShopProject.model.User;
import com.missMirite.onlineShopProject.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.missMirite.onlineShopProject.utils.DataPopulator.populateNew;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    private final JwtService service;

    private final AuthenticationManager authenticationManager;

    public UserDTO register(RegisterRequest request) {
        if (!isEmailAlreadyRegistered(request.getEmail())) {
            User user = User.builder()
                    .firstName(request.getFirstName())
                    .lastName(request.getLastName())
                    .email(request.getEmail())
                    .password(passwordEncoder.encode(request.getPassword()))
                    .role(request.getRole())
                    .build();
            repository.saveAndFlush(user);
            return populateNew(user, UserDTO.class);
        } else throw new UserAlreadyExistsException("User with this email is already registered!");
    }

    private boolean isEmailAlreadyRegistered(String email) {
        return repository.findAll().stream().anyMatch(u -> u.getUsername().equals(email));
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
            User user = (User) repository.findUserByEmail(request.getEmail());
            var jwtToken = service.generateToken(user);
            return AuthenticationResponse.builder()
                    .token(jwtToken)
                    .role(user.getRole().toString())
                    .message("You were successfully logged in!")
                    .build();
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("It seems like email or password you entered is wrong!");
        }
    }


}
