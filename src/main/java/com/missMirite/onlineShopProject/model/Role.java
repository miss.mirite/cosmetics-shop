package com.missMirite.onlineShopProject.model;

public enum Role {
    USER,
    ADMIN
}
