package com.missMirite.onlineShopProject.model;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "beautyProducts")
public class BeautyProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "productName")
    private String productName;

    @Column(name = "manufacturerName")
    private String manufacturerName;

    @Column(name = "productType")
    private String productType;

    @Column(name = "price")
    private BigDecimal price;

    @ManyToMany(mappedBy = "orderItems")
    private List<Order> orders;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BeautyProduct beautyProduct = (BeautyProduct) o;
        return id.equals(beautyProduct.id)
                && productName.equals(beautyProduct.productName)
                && manufacturerName.equals(beautyProduct.manufacturerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productName, manufacturerName);
    }

}

