package com.missMirite.onlineShopProject.model;

import com.missMirite.onlineShopProject.dto.BeautyProductsDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class ShoppingCart {

    private Integer id;

    private User user;

    private List<BeautyProductsDTO> cartItems = new ArrayList<>();

    public void addItem(BeautyProductsDTO beautyProduct) {
        cartItems.add(beautyProduct);
    }

    public void deleteItem(BeautyProductsDTO beautyProduct) {
        cartItems.remove(beautyProduct);
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<BeautyProductsDTO> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<BeautyProductsDTO> cartItems) {
        this.cartItems = cartItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCart shoppingCart = (ShoppingCart) o;
        return id.equals(shoppingCart.id)
                && user.equals(shoppingCart.user)
                && cartItems.equals(shoppingCart.cartItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, cartItems);
    }
}
